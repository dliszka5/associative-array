#include <iostream>

#include "unordered_map.h"

using namespace std;

int main() {
	typedef string key_type;
	TMap<key_type,string,default_hash_function> ob(5);

	ob["author"] = "Dawid Liszka";
	ob["brother"] = "Dariusz Liszka";
	ob["likes"]	= "sernik";
	ob["music"] = "Rock";
	
	cout << ob["author"] << endl;
	ob["author"] = "DD";

	// data in ob["author"] changed

	cout << ob["author"] << endl;
	cout << ob["likes"] << endl;
	cout << ob["brother"] << endl;

	cout << ob.bucket_size(0) << endl;

	//cout << "Testing something that doesnt exists : " ;
	//cout << ob["something"] << endl;	
	
	// if something doesnt exists TMAP creating default object of given type

	cout << ob["brother"] << endl;
	ob.erase("author");
	cout << endl << ob["author"];

	cout << endl << "..:: START ITERATOR LOCAL TEST ::.." << endl;

	TMap<key_type,string,default_hash_function>::local_iterator local_begin = ob.begin(0);

	for(local_begin; local_begin != ob.end(0); local_begin++){
		cout << *local_begin << endl;
	}

	cout << "..:: END ITERATOR LOCAL TEST ::.." << endl;


	cout << endl << "..:: START ITERATOR TEST ::.." << endl;

	TMap<key_type,string,default_hash_function>::iterator iterator_begin = ob.begin();
	//TMap<key_type,string,default_hash_function>::iterator iterator_end = ob.end();

	for(iterator_begin; iterator_begin != ob.end(); ++iterator_begin){
		cout << *iterator_begin << endl;
	}

	cout << "..::END ITERATOR TEST ::.." << endl;

	return 0;
}