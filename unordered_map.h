/*
 *	Author	: Dawid Liszka
 *  Project	: Unordered associative map 
**/

#include <vector>

template<typename T>
int default_hash_function(const T& key, const int size) {
	int length = key.length();
	int hash_sum = 0;

	for(int i=0; i<length; ++i) {
		hash_sum+=key[i];
	}
	return hash_sum % size;
}

template<typename T>
int my_hash_function(const T& key, const int size) {
	int length = key.length();
	int hash_sum = 0;

	for(int i=0; i<length; ++i) {
		hash_sum+=key[i];
	}
	
	int RETURN = hash_sum%size;
	RETURN = RETURN%13;
	RETURN = hash_sum%size + RETURN;
	if(RETURN >= size) {
		RETURN = size-1;
	}
	return RETURN;
}


/**
 * TMap is container for data, where we put/get data via hashed values
 * TMap["Author"] = "Dawid Liszka"  
 *
 * @param  T        type of key
 * @param  U        type of data
 * @param  ptrFun   pointer to hash function
 * @return          data
 */

template<typename T, typename U, int (*ptrFun)(const T&, const int) = default_hash_function>
class TMap {
public:
	friend class iterator;
	struct List {
		List* next;
		T key;
		U data;
	};

private:
	List** head;			
	int size;
	int howMany;
	bool* bucketUsed;
public:
	TMap();
	TMap(int);

	U& operator[] (const T& keyword) {
		int index = ptrFun(keyword,size);
		List* list = head[index];
		List* e = head[index];
		bucketUsed[index] = true;

		while(list && list->key != keyword) {
      list = list->next;
    }

		if(list) {
			howMany++;
			return list->data;
		} else {				
			List* buff = new List;
			buff->next = nullptr;
			buff->key = keyword;
			U object;
			buff->data = object;
			if(head[index] == nullptr) {
				head[index] = buff;
			} else {
				while(e->next) {
          e = e->next;
        }
				e->next = buff;
			}

			howMany++;
			return buff->data;
		}
	}

	U& insert(const T& keyword, U data) {
		int index = ptrFun(keyword,size);
		List* list;

		List* buff = new List;
		buff->next = nullptr;
		buff->data = data;
		buff->key = keyword;

		list = head[index];
		if(list) {
			while(list->next) {
        list = list->next;
      }
			list->next = buff;
		} else {
			head[index] = buff;
		}
		++howMany;
		return *buff;
	}

	 U& get(const T& keyword) {
		int index = ptrFun(keyword,size);
		List* list = head[index];

		while(list && list->key != keyword) {
      list = list->next;
    }
		if(list) {
			return list->data;
		}
		U* object = new U();
		return *object;
	}

	int bucket_count() {
		int count = 0;
		List& list = head;
		for(int i = 0; i<size; ++i) {
			if(head[i]) {
				++count;
      }
		}
		return count;
	}

	int max_bucket_count() {
		return size;
	}

	int bucket_size(int index) {
		int count = 0;
		if(head[index]) {
			List* buf = head[index];
			while(buf) {
				buf = buf->next;
				++count;
			}
		}
		return count;
	}

	int bucket_size(const T& keyword) {
		int count = 0;
		int index = ptrFun(keyword,size);
		if(head[index]) {
			List* buf = head[index];
				while(buf) {
					buf = buf->next;
					++count;
				}
		}
		return count;
	}

	List& bucket(int index) {
		return head[index];
	}

	bool erase(const T& keyword) {
		int index = ptrFun(keyword,size);
		List* list = head[index];
		List* prev = head[index];
		while(list && list->key != keyword) {
			if(list->next->key == keyword) {
				prev = list;
			}
			list = list->next;
		}

		if(list) {	
			if(list->next != nullptr) {
				prev->next = list->next;
			}

			if(list == head[index]) {
				List* buf = list->next;
				if(list->next == nullptr) {
					bucketUsed[index] == false;
				}
				head[index] = buf;
			}
			delete list;
			list = nullptr;
			return true;
		}
		return false;
	}

	class local_iterator {
		List* current;
	public:
		local_iterator(List* bucket = nullptr) : current(bucket) {}

		local_iterator(const local_iterator& object) {
			this->current = object.current;
		}

		local_iterator& operator++() {
			current = current->next;
			return *this;
		}

		local_iterator& operator++(int) {
			local_iterator* buff = new local_iterator(*this);
			current = current->next;
			return *buff;
		}

		U& operator*() {
			return current->data;
		}

		U* operator->() {
			return &current->data;
		}

		bool operator==(const local_iterator& object) {
			if(current == object.current) {
        return true;
      }
			return true;
		}

		bool operator!=(const local_iterator& object) {
			if(this->current != object.current) {
        return true;
      }
			return false;
		}
	};

	class iterator {
	private:
		friend TMap;
		List** elements;
		List* currentBucket;
		List* current;
	public:

		iterator() {
			elements = nullptr;
			currentBucket = nullptr;
			current = nullptr;
		}

		iterator(List** init = nullptr) {
			elements = init;
			currentBucket = elements[0];
			current = elements[0];
		}

		iterator(const iterator& object) {
			elements = object.elements;
			current = object.current;
			currentBucket = object.currentBucket;
		}
 
		iterator(List* end = nullptr) {
			current = end;
		}

		bool goToNextBucket() {
			static int bucket = 0;
			bucket++;
			if(elements[bucket]) {
				currentBucket = elements[bucket];
				current = elements[bucket];
				return true;
			} else {
				bucket = 0;
				current = current->next;
				return false;
			}
		}

		bool erase() {
			iterator* buf = iterator(*this);
			buf++;
			delete buf->current;
		}

		iterator& operator++() {
			if(current->next) {
				current = current->next;
			} else {
				goToNextBucket();
			}
			return *this;
		}

		iterator& operator++(int) {
			local_iterator* buff = new local_iterator(*this);
			
			if(current->next) {
				current = current->next;
			} else {
				goToNextBucket();
			}
			return *buff;
		}

		U& operator*() {
			return current->data;
		}

		U* operator->() {
			return &current->data;
		}

		bool operator==(const iterator& object) {
			if(current == object.current) {
        return true;
      }
			return false;
		}

		bool operator!=(const iterator& object) {
			if(current != object.current) {
        return true;
			}
			return false;
		}
	};


	iterator& begin() {
  	int k = 0;
  	List** buf = new List*[size];
  	for(int i = 0; i < size; ++i) {
  		if(head[i]) {
  			buf[k++] = head[i];
      }
  	}

  	List* _list = buf[0];
  	k = 0;

  	while(_list) {
  		if(_list->next == nullptr) {
  			_list = buf[++k];
  		} else {
  			_list = _list->next;
  		}
  	}

  	iterator* iterator_begin = new iterator(buf);
  	return *iterator_begin;
	}

	iterator& end() {
		int index = 0;
		for(int i = 0; i < size; ++i) {
			if(bucketUsed[i] == true) {
				index = i;
			}
		}

		List* buf = head[index];
		while(buf->next) {
			buf = buf->next;
		}
		buf = buf->next;

		iterator* iterator_end = new iterator(buf);
		return *iterator_end;
	}

	local_iterator& begin(int index) {
		local_iterator* local_begin = new local_iterator(head[index]);
		return *local_begin;
	}

	local_iterator& end(int index) {
		List* buf = head[index];
		if(head[index]) {
				while(buf->next) {
					buf = buf->next;
				}
				buf = buf->next;
		}
		local_iterator* local_end = new local_iterator(buf);
		return *local_end;
	}

	~TMap();
};

template<typename T, typename U, int (*ptrFun)(const T&, const int)>
TMap<T,U,ptrFun>::TMap() {
	this->size = 1;
	this->howMany = 0;
	head = new List*[size];
	bucketUsed = new bool [size];
}

template<typename T, typename U,int (*ptrFun)(const T&,const int)>
TMap<T,U,ptrFun>::TMap(int size) {
	this->size = size;
	this->howMany = 0;
	head = new List*[size];
	bucketUsed = new bool [size];
}

template<typename T,typename  U,int (*ptrFun)(const T&, const int)>
TMap<T,U,ptrFun>::~TMap() {
	for(int i = 0; i < size; ++i) {
		if(head[i]) {
			List* list = head[i];
			std::vector< List* > prev;
			while(list->next) {
				prev.push_back(list);
				list = list->next;
			}
			prev.push_back(list);
			for(int k = prev.size() - 1; k >= 0; --k) {
				delete prev[k];
				prev[k] = nullptr;	
			}
		}
	}

	delete [] head;
	head = nullptr;
	delete [] bucketUsed;
	bucketUsed = nullptr;
}
