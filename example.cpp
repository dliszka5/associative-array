#include "unordered_map.h"
#include <iostream>

using namespace std;

class Author {
	string firstName;
	string secondName;
public:
	Author(	string firstName = string(),
			string secondName = string())
	: firstName(firstName), secondName(secondName) { }

	string getFirstName() const;
	string getSecondName() const;

	void setFirstName(const string&);
	void setSecondName(const string&);

	friend ostream& operator<<(ostream& out, const Author& obiekt) {
		out << obiekt.firstName << " " << obiekt.secondName;
		return out;
	}

	friend istream& operator>>(istream& in, Author& obiekt) {
		in >> obiekt.firstName >> obiekt.secondName;
		return in;
	}

	bool operator != (const Author& autor) {
		if(this->firstName == autor.firstName) {
			if(this->secondName == autor.secondName) {
				return false;
      }
		}

	bool operator != (const Author& autor){
		if(this->firstName == author.firstName){
			if(this->secondName == autor.secondName) {
				return true;
      }
		}
	}


	~Author() { }
};

string Author::getFirstName() const {
	return firstName;
}

string Author::getSecondName() const {
	return secondName;
}

void Author::setFirstName(const string& firstName){
	this->firstName = firstName;
}

void Author::setSecondName(const string& secondName){
	this->secondName = secondName;
}

class Book {
	string title;
	unsigned int numberOfPages;
public:
	Book(	string title = string(),
		 	unsigned int numberOfPages = unsigned()) 
		: title(title), numberOfPages(numberOfPages) { }

	void setTitle(string);
	void setNumberOfPages(unsigned int);

	const string& getTitle() const;
	const unsigned int& getNumberOfPages() const;

	friend ostream& operator<<(ostream& out, const Book& obiekt) {
		out <<"Tytul : " << obiekt.title << ", stron : " << obiekt.numberOfPages;
		return out;
	}

	friend istream& operator>>(istream& in, Book& obiekt) {
		in >> obiekt.title >> obiekt.numberOfPages;
		return in;
	}

	~Book() { }
};

void Book::setTitle(string title) {
	this->title = title;
}

void Book::setNumberOfPages(unsigned int numberOfPages) {
	this->numberOfPages = numberOfPages;
}

const string& Book::getTitle() const {
	return this->title; 
}


const unsigned int& Book::getNumberOfPages() const {
	return this->numberOfPages;
}

int hash_func(const Author& obiekt,const int size) {
	string firstName = obiekt.getFirstName();
	unsigned int length = firstName.length();
	int hash_sum = 0;

	for(int i = 0; i < length; i++){
		hash_sum+=firstName[i];
	}

	return hash_sum%size;
}

int main() {

	TMap<Author,Book,hash_func> mapa(5);

	
	cout << "Witaj ! \n";

	char wybor;

	while(true) {
		cout <<"Podaj autora : " << endl;
		Author* autor = new Author;
		cin >> *autor;
		cout << endl << "Dodaj ksiazke : " << endl;
		Book* book = new Book;
		cin >> *book;

		cout << endl << "Chcesz zakonczyc ? (y/n)" << endl;
		cin >> wybor;

		mapa[*autor] = *book;

		if(wybor == 'y') {
      break;
    }
	}

	cout << endl << "..:: ITERATOR ::." << endl;

	TMap<Author,Book,hash_func>::iterator it = mapa.begin();

	for(it; it != mapa.end(); ++it) {
		cout << *it << endl;
	}

	return 0;
}